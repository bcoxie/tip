package com.tip;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class SpiderMesh implements InputProcessor
{
	public static final int TOP_LEFT = 0;
	public static final int TOP_RIGHT = 1;
	public static final int BOTTOM_LEFT = 2;
	public static final int BOTTOM_RIGHT = 3;
	public static final int CENTER = 4;
	public static final int EXTENDED_TOP_LEFT = 5;
	public static final int EXTENDED_TOP_RIGHT = 6;
	public static final int EXTENDED_BOTTOM_LEFT = 7;
	public static final int EXTENDED_BOTTOM_RIGHT = 8;
	
	private int numMovablePoints = 5;
	private float pointWidth = 0.03f;
	private float pointHeight = pointWidth;
	private float halfPWidth = pointWidth / 2;
	private float halfPHeight = pointHeight / 2;
	//private float cornerLineLength = 1.0f;
	
	private Vector2[] meshPoints = {new Vector2(-0.2f, 0.2f), new Vector2(0.2f, 0.2f),
			new Vector2(-0.2f, -0.2f), new Vector2(0.2f, -0.2f), new Vector2(0, 0),
			new Vector2(-0.3f, 0.3f), new Vector2(0.3f, 0.3f), new Vector2(-0.3f, -0.3f), new Vector2(0.3f, -0.3f)};
	
	private Sprite currImage;
	private Rectangle defaultRect = new Rectangle(-0.5f, -0.5f, 1.0f, 1.0f);
	private Rectangle meshPointColRect = new Rectangle(0, 0, pointWidth, pointHeight);
	private Camera camera;
	private Vector3 touchPoint = new Vector3();
	private int draggingMeshPoint;
	
	public SpiderMesh(Camera cam)
	{
		this.camera = cam;
		currImage = null;
		draggingMeshPoint = -1;
	}
	
	/**
	 * Set current image to bind spider mesh to
	 * @param sprite image to bind spider mesh to
	 */
	public void setImage(Sprite sprite)
	{
		currImage = sprite;
		
		Rectangle imageRect = currImage.getBoundingRectangle();
		
		float imageLeft = imageRect.x;
		float imageRight = imageRect.x + imageRect.width;
		float imageTop = imageRect.y + imageRect.height;
		float imageBottom = imageRect.y;
		
		meshPoints[TOP_LEFT].x = imageLeft * 0.5f;
		meshPoints[TOP_LEFT].y = imageTop * 0.5f;
		
		meshPoints[TOP_RIGHT].x = imageRight * 0.5f;
		meshPoints[TOP_RIGHT].y = imageTop * 0.5f;

		meshPoints[BOTTOM_LEFT].x = imageLeft * 0.5f;
		meshPoints[BOTTOM_LEFT].y = imageBottom * 0.5f;
		
		meshPoints[BOTTOM_RIGHT].x = imageRight * 0.5f;
		meshPoints[BOTTOM_RIGHT].y = imageBottom * 0.5f;
		
		meshPoints[CENTER].x = imageRect.x + (imageRect.width * 0.5f);
		meshPoints[CENTER].y = imageRect.y + (imageRect.height * 0.5f);
		
		updateExtendedPoints();
	}
	
	/**
	 * Get position of mesh point relative to current sprite
	 * @param meshPoint e.g., TOP_LEFT, EXTENDED_BOTTOM_RIGHT, CENTER
	 * @return position of mesh point in sprite space: (0, 0) = bottom left of sprite, width = 1, height = 1
	 */
	public Vector2 getMeshPointTexCoords(int meshPoint)
	{
		Vector2 texCoords = meshPoints[meshPoint].cpy();
		Rectangle imageRect = currImage.getBoundingRectangle();
		
		texCoords.x = (texCoords.x - imageRect.x) / imageRect.width;
		texCoords.y = (texCoords.y - imageRect.y) / imageRect.height;
		
		return texCoords;
	}
	
	/**
	 * Render the spider mesh
	 * @param shapeRenderer to be used for drawing lines and points
	 */
	public void render(ShapeRenderer shapeRenderer)
	{	
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(0, 1, 1, 1);
		shapeRenderer.line(meshPoints[TOP_LEFT], meshPoints[TOP_RIGHT]);
		shapeRenderer.line(meshPoints[TOP_RIGHT], meshPoints[BOTTOM_RIGHT]);
		shapeRenderer.line(meshPoints[BOTTOM_RIGHT], meshPoints[BOTTOM_LEFT]);
		shapeRenderer.line(meshPoints[BOTTOM_LEFT], meshPoints[TOP_LEFT]);
		
		shapeRenderer.line(meshPoints[CENTER], meshPoints[EXTENDED_TOP_RIGHT]);
		shapeRenderer.line(meshPoints[CENTER], meshPoints[EXTENDED_BOTTOM_RIGHT]);
		shapeRenderer.line(meshPoints[CENTER], meshPoints[EXTENDED_BOTTOM_LEFT]);
		shapeRenderer.line(meshPoints[CENTER], meshPoints[EXTENDED_TOP_LEFT]);
		
		shapeRenderer.setColor(1.0f, 0.2f, 0.2f, 1.0f);
		shapeRenderer.line(meshPoints[EXTENDED_TOP_LEFT], meshPoints[EXTENDED_TOP_RIGHT]);
		shapeRenderer.line(meshPoints[EXTENDED_TOP_RIGHT], meshPoints[EXTENDED_BOTTOM_RIGHT]);
		shapeRenderer.line(meshPoints[EXTENDED_BOTTOM_RIGHT], meshPoints[EXTENDED_BOTTOM_LEFT]);
		shapeRenderer.line(meshPoints[EXTENDED_BOTTOM_LEFT], meshPoints[EXTENDED_TOP_LEFT]);
		shapeRenderer.end();
		
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(0.2f, 1, 0.2f, 1);
		
		drawPointRect(meshPoints[TOP_LEFT], shapeRenderer);
		drawPointRect(meshPoints[TOP_RIGHT], shapeRenderer);
		drawPointRect(meshPoints[BOTTOM_LEFT], shapeRenderer);
		drawPointRect(meshPoints[BOTTOM_RIGHT], shapeRenderer);
		drawPointRect(meshPoints[CENTER], shapeRenderer);
		shapeRenderer.end();
	}
	
	private float calcLineLength()
	{
		Rectangle imageRect = defaultRect;
		if (currImage != null)
		{
			imageRect = currImage.getBoundingRectangle();
		}
		
		float imageLeft = imageRect.x;
		float imageRight = imageRect.x + imageRect.width;
		float imageTop = imageRect.y + imageRect.height;
		float imageBottom = imageRect.y;
		
		float cornerDist1 = minLineLength(meshPoints[CENTER], meshPoints[TOP_LEFT], imageLeft, imageTop);
		float cornerDist2 = minLineLength(meshPoints[CENTER], meshPoints[TOP_RIGHT], imageRight, imageTop);
		float cornerDist3 = minLineLength(meshPoints[CENTER], meshPoints[BOTTOM_LEFT], imageLeft, imageBottom);
		float cornerDist4 = minLineLength(meshPoints[CENTER], meshPoints[BOTTOM_RIGHT], imageRight, imageBottom);
		
		return Math.max(Math.max(cornerDist1, cornerDist2), Math.max(cornerDist3, cornerDist4));
	}
	
	private float minLineLength(Vector2 from, Vector2 through, float minX, float minY)
	{
		float xLength;
		float yLength;
		
		Vector2 lineDir = through.cpy().sub(from);
		lineDir = lineDir.nor();
		
		xLength = Math.abs((minX - from.x) / lineDir.x); 	// min line length necessary to intersect minX
		yLength = Math.abs((minY - from.y) / lineDir.y);	// min line length necessary to intersect minY
		
		return Math.max(xLength, yLength);
	}
	
	private void drawPointRect(Vector2 point, ShapeRenderer sRenderer)
	{
		sRenderer.rect(point.x - halfPWidth, point.y - halfPHeight, pointWidth, pointHeight);
	}
	
	private void bindPoints(int movedPoint)
	{
		Rectangle imageRect = currImage.getBoundingRectangle();
		
		float imageLeft = imageRect.x;
		float imageRight = imageRect.x + imageRect.width;
		float imageTop = imageRect.y + imageRect.height;
		float imageBottom = imageRect.y;
		
		// adjust point positions
		switch (movedPoint)
		{
			case TOP_LEFT:
				meshPoints[TOP_LEFT].x = Math.max(meshPoints[TOP_LEFT].x, imageLeft);
				meshPoints[TOP_LEFT].x = Math.min(meshPoints[TOP_LEFT].x, imageRight - pointWidth * 2);
				meshPoints[TOP_LEFT].y = Math.max(meshPoints[TOP_LEFT].y, imageBottom + pointHeight * 2);
				meshPoints[TOP_LEFT].y = Math.min(meshPoints[TOP_LEFT].y, imageTop);
				
				meshPoints[BOTTOM_LEFT].x = meshPoints[TOP_LEFT].x;
				meshPoints[TOP_RIGHT].y = meshPoints[TOP_LEFT].y;
				
				meshPoints[CENTER].x = Math.max(meshPoints[CENTER].x, meshPoints[TOP_LEFT].x + pointWidth);
				meshPoints[CENTER].y = Math.min(meshPoints[CENTER].y, meshPoints[TOP_LEFT].y - pointHeight);
				break;
			case TOP_RIGHT:
				meshPoints[TOP_RIGHT].x = Math.max(meshPoints[TOP_RIGHT].x, imageLeft + pointWidth * 2);
				meshPoints[TOP_RIGHT].x = Math.min(meshPoints[TOP_RIGHT].x, imageRight);
				meshPoints[TOP_RIGHT].y = Math.max(meshPoints[TOP_RIGHT].y, imageBottom + pointHeight * 2);
				meshPoints[TOP_RIGHT].y = Math.min(meshPoints[TOP_RIGHT].y, imageTop);
				
				meshPoints[BOTTOM_RIGHT].x = meshPoints[TOP_RIGHT].x;
				meshPoints[TOP_LEFT].y = meshPoints[TOP_RIGHT].y;
				
				meshPoints[CENTER].x = Math.min(meshPoints[CENTER].x, meshPoints[TOP_RIGHT].x - pointWidth);
				meshPoints[CENTER].y = Math.min(meshPoints[CENTER].y, meshPoints[TOP_RIGHT].y - pointHeight);
				break;
			case BOTTOM_LEFT:
				meshPoints[BOTTOM_LEFT].x = Math.max(meshPoints[BOTTOM_LEFT].x, imageLeft);
				meshPoints[BOTTOM_LEFT].x = Math.min(meshPoints[BOTTOM_LEFT].x, imageRight - pointWidth * 2);
				meshPoints[BOTTOM_LEFT].y = Math.max(meshPoints[BOTTOM_LEFT].y, imageBottom);
				meshPoints[BOTTOM_LEFT].y = Math.min(meshPoints[BOTTOM_LEFT].y, imageTop - pointHeight * 2);
				
				meshPoints[TOP_LEFT].x = meshPoints[BOTTOM_LEFT].x;
				meshPoints[BOTTOM_RIGHT].y = meshPoints[BOTTOM_LEFT].y;
				
				meshPoints[CENTER].x = Math.max(meshPoints[CENTER].x, meshPoints[BOTTOM_LEFT].x + pointWidth);
				meshPoints[CENTER].y = Math.max(meshPoints[CENTER].y, meshPoints[BOTTOM_LEFT].y + pointHeight);
				break;
			case BOTTOM_RIGHT:
				meshPoints[BOTTOM_RIGHT].x = Math.max(meshPoints[BOTTOM_RIGHT].x, imageLeft + pointWidth * 2);
				meshPoints[BOTTOM_RIGHT].x = Math.min(meshPoints[BOTTOM_RIGHT].x, imageRight);
				meshPoints[BOTTOM_RIGHT].y = Math.max(meshPoints[BOTTOM_RIGHT].y, imageBottom);
				meshPoints[BOTTOM_RIGHT].y = Math.min(meshPoints[BOTTOM_RIGHT].y, imageTop - pointHeight * 2);
				
				meshPoints[TOP_RIGHT].x = meshPoints[BOTTOM_RIGHT].x;
				meshPoints[BOTTOM_LEFT].y = meshPoints[BOTTOM_RIGHT].y;
				
				meshPoints[CENTER].x = Math.min(meshPoints[CENTER].x, meshPoints[BOTTOM_RIGHT].x - pointWidth);
				meshPoints[CENTER].y = Math.max(meshPoints[CENTER].y, meshPoints[BOTTOM_RIGHT].y + pointHeight);
				break;
			case CENTER:
				meshPoints[CENTER].x = Math.max(meshPoints[CENTER].x, imageLeft + pointWidth);
				meshPoints[CENTER].x = Math.min(meshPoints[CENTER].x, imageRight - pointWidth);
				meshPoints[CENTER].y = Math.max(meshPoints[CENTER].y, imageBottom + pointHeight);
				meshPoints[CENTER].y = Math.min(meshPoints[CENTER].y, imageTop - pointHeight);
				break;
		}
		
		// adjust for center point position
		meshPoints[TOP_LEFT].x = Math.min(meshPoints[TOP_LEFT].x, meshPoints[CENTER].x - pointWidth);
		meshPoints[TOP_LEFT].y = Math.max(meshPoints[TOP_LEFT].y, meshPoints[CENTER].y + pointHeight);
		meshPoints[TOP_RIGHT].x = Math.max(meshPoints[TOP_RIGHT].x, meshPoints[CENTER].x + pointWidth);
		meshPoints[TOP_RIGHT].y = Math.max(meshPoints[TOP_RIGHT].y, meshPoints[CENTER].y + pointHeight);
		meshPoints[BOTTOM_LEFT].x = Math.min(meshPoints[BOTTOM_LEFT].x, meshPoints[CENTER].x - pointWidth);
		meshPoints[BOTTOM_LEFT].y = Math.min(meshPoints[BOTTOM_LEFT].y, meshPoints[CENTER].y - pointHeight);
		meshPoints[BOTTOM_RIGHT].x = Math.max(meshPoints[BOTTOM_RIGHT].x, meshPoints[CENTER].x + pointWidth);
		meshPoints[BOTTOM_RIGHT].y = Math.min(meshPoints[BOTTOM_RIGHT].y, meshPoints[CENTER].y - pointHeight);
	}
	
	private void updateExtendedPoints()
	{
		float lineLength = calcLineLength();
		
		meshPoints[EXTENDED_TOP_LEFT] = getEndPoint(meshPoints[CENTER], meshPoints[TOP_LEFT], lineLength);
		meshPoints[EXTENDED_TOP_RIGHT] = getEndPoint(meshPoints[CENTER], meshPoints[TOP_RIGHT], lineLength);
		meshPoints[EXTENDED_BOTTOM_LEFT] = getEndPoint(meshPoints[CENTER], meshPoints[BOTTOM_LEFT], lineLength);
		meshPoints[EXTENDED_BOTTOM_RIGHT] = getEndPoint(meshPoints[CENTER], meshPoints[BOTTOM_RIGHT], lineLength);
	}
	
	private Vector2 getEndPoint(Vector2 from, Vector2 through, float length)
	{
		Vector2 endPoint;
		Vector2 lineDir = through.cpy().sub(from);
		lineDir = lineDir.nor();
		lineDir = lineDir.scl(length);
		
		endPoint = from.cpy().add(lineDir);
		
		return endPoint;
	}

	
	///// Input /////
	
	@Override
	public boolean keyDown(int keycode)
	{
		return false;
	}

	@Override
	public boolean keyUp(int keycode)
	{
		return false;
	}

	@Override
	public boolean keyTyped(char character)
	{
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button)
	{
		touchPoint.set(screenX, screenY, 0);
		camera.unproject(touchPoint);
		
		for (int i = 0; i < numMovablePoints; i++)
		{
			meshPointColRect.x = meshPoints[i].x - meshPointColRect.width * 0.5f;
			meshPointColRect.y = meshPoints[i].y - meshPointColRect.height * 0.5f;
			
			if (meshPointColRect.contains(touchPoint.x, touchPoint.y))
			{
				draggingMeshPoint = i;
				
				meshPoints[i].x = touchPoint.x;
				meshPoints[i].y = touchPoint.y;
				
				bindPoints(i);
				updateExtendedPoints();
			}
		}
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		draggingMeshPoint = -1;
		
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		touchPoint.set(screenX, screenY, 0);
		camera.unproject(touchPoint);
		
		if (draggingMeshPoint >= 0)
		{
			meshPoints[draggingMeshPoint].x = touchPoint.x;
			meshPoints[draggingMeshPoint].y = touchPoint.y;
			
			bindPoints(draggingMeshPoint);
			updateExtendedPoints();
		}
		
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		return false;
	}

	@Override
	public boolean scrolled(int amount)
	{
		return false;
	}
}
